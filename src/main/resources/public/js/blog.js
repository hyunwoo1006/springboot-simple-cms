

$(document).ready(function()
{

   $("#articles_container").css("min-height", "10em")

    // TODO : User angular-js
   $( "#sidebar-button" ).click(function() {
      $('.ui.sidebar')
        .sidebar('setting', 'dimPage', false)
        .sidebar('toggle')
      ;
   });

    // TODO : Use angular-js
   $( ".ui.sidebar.menu #login" ).click(function() {
      $('#login-modal')
        .modal('show');

   });

   // TODO : User angular-js
   $( ".ui.sidebar.menu #logout" ).click(function() {
         $('#logout-modal')
           .modal('show');
      });

   $('#login-modal')
        .modal({
            closable : false,
            onApprove : function() {
                return false;
            },
            onHide : modalOnHide
   });


});

function modalOnHide() {
    $('#login-error-msg-box').addClass("hidden");
    $("input[name='password']").val("");
    $("input[name='username']").val("");
}
