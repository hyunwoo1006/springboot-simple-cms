var m = angular.module('blogModule', [

    'ngRoute',
    'blogModule.ArticleController',
    'blogModule.SecurityController',
    'blogModule.ArticleService',
    'blogModule.SecurityService'

]);

m.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/articles', {
        templateUrl: 'view/articles.html',
        controller: 'ArticleController'
      }).
      when('/saveArticle', {
        templateUrl: 'view/saveArticle.html',
        controller: 'ArticleController'
      }).
      when('/editArticle', {
        templateUrl: 'view/editArticle.html',
        controller: 'ArticleController'
      }).
      otherwise({
        redirectTo: '/articles'
      });
  }]);