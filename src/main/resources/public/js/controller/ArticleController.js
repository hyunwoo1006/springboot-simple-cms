angular.module('blogModule.ArticleController', []).

controller('ArticleController', function($scope, $sce, $location, ArticleService) {

    $scope.currentEditArticleId;

    // If websocket codes go into init method. Things doesn't work. WTF? Need to learn more about javascript structure.
    var articleSocket = new WebSocket("ws://localhost:4000/ws/article");
    articleSocket.onmessage = function (event) {
//        console.log(event.data);

        var data = JSON.parse(event.data);

        $scope.updateComment(data.articleId, data.comment);
    }

    $scope.updateComment = function(articleId, comment) {
        console.log("updating comment.... articleId=" + articleId, "comment=" + comment);
        for(var i = 0; i < $scope.articles.length; i++) {
            if($scope.articles[i].id == articleId) {
                console.log("article found... id=" + $scope.articles[i].id);

                found = false;

                for(var j = 0; j < $scope.articles[i].comments.length; j++) {

                    if(comment.id == $scope.articles[i].comments[j].id) {
                        console.log("Comment Id found. this is an update of existing comment. CommentId=" + comment.id);
                        $scope.articles[i].comments[j] = comment;
                        found = true;
                        break;
                    }

                }

                if(!found) {
                    console.log("This is a new comment. Adding one now. Comment=" + JSON.stringify(comment));
                    $scope.articles[i].comments.push(comment);
                    //  Sorting becomes real expensive operation as data grows.....
                    $scope.articles[i].comments.sort(function(a,b) {
                        return b.created - a.created
                    });
                    $scope.$apply();
                }

                break;
            }
        }
    }

    $scope.getArticles = function() {
        ArticleService.getArticles().
            then(function(data) {
//                alert(JSON.stringify(data));


                // TODO : Need to handle error cases....... this code is on assumption that request is success
                // TODO : Make a seperate helper for sorting...
//                alert(JSON.stringify(data.data.result));
                // For articles.. newer ones come top
                data.data.result.sort(function(a,b) {
                    return b.created - a.created;
                })
//                alert(JSON.stringify(data.data.result));
                for(i in data.data.result) {
//                    console.log(JSON.stringify(data.data.result[i]));
                    // For comments. Newer ones come bottom
                    data.data.result[i].comments.sort(function(a,b) {
                        return b.created - a.created
                    })
                }
//                alert(JSON.stringify(data.data.result));


                $scope.articles = data.data.result;
                $("#articles_container .dimmer").removeClass("active");
            });

    }

    $scope.saveComment = function(articleId, commentInput) {


        $('#commentSaveBtn' + articleId).button('loading')
//        $("#comment-submit-button-" + articleId).addClass("loading");
//        content = $("textarea#" +  articleId + "-comment-content" ).val();

        if(commentInput == undefined || commentInput == '') {
            alert("provide content");
//            $("#comment-submit-button-" + articleId).removeClass("loading");
        $('#commentSaveBtn' + articleId).button('reset')
            return;
        }

//        console.log("saveComment triggered from ArticleController");

        ArticleService.saveComment(articleId, commentInput).
            then(function(data) {
//                alert(JSON.stringify(data));

//                $("#comment-submit-button-" + articleId).removeClass("loading");
                $('#commentSaveBtn' + articleId).button('reset')
            })

    }

    $scope.formatTimestamp = function(currentTime) {
        return convertToFormattedDateTime(currentTime);
    }

    $scope.formatTimestampFormat = function(currentTime, format) {
            return convertToFormattedDateTimeFormat(currentTime, format);
        }

    $scope.expandReply = function() {

    }

    $scope.test = function() {

        json='{"id":4,"created":1439538177283,"updated":1439538177283,"createdBy":"administrator","content":"comment~"}'
        parsedObj = JSON.parse(json);
        alert("test button : " + JSON.stringify(parsedObj));

        $scope.articles[0].comments.push(parsedObj);
    }

    $scope.articlesInit = function() {

        console.log('init')

        $scope.getArticles();
    }

    $scope.deleteArticle = function(articleId) {

        var confirm = window.confirm("You are about to delete an article.. are you sure?");
        if(confirm == false) {
            return;
        }
        var confirm2 = window.confirm("Are you really really sure?");
        if(confirm2 == false) {
            return;
        }


        ArticleService.deleteArticle(articleId).
            then(function(data) {
                alert(JSON.stringify(data));
                console.log(JSON.stringify(data));
                $scope.getArticles();
            });

    }

    $scope.createArticle = function(title) {

        if(title == undefined || title == "") {
            return;
        }

        $('#article-save-button').button('loading')

//        alert(JSON.stringify(title));
//        alert(JSON.stringify( $("#textEditor").code()));

        ArticleService.createArticle(title, $("#textEditor").code()).
            then(function(response) {
                alert(JSON.stringify(response));
                $('#article-save-button').button('reset')
//                console.log(JSON.stringify(response));
//                alert(title);
//                alert(content);
            }, function(response) {

                alert(JSON.stringify(response));
                $('#article-save-button').button('reset')


            });
    }

    $scope.editArticle = function(title) {


        $('#article-edit-button').button('loading')

        ArticleService.editArticle($scope.editArticleId, title, $("#textEditor").code()).
            then(function(response) {
//                alert(JSON.stringify(response));

                $('#article-edit-button').button('reset')
                $location.path('/articles')
            });

    }

    $scope.editArticleView = function(article) {

//        alert(JSON.stringify($scope.editArticleData))

        ArticleService.setEditArticleData(article);
//        alert(JSON.stringify(ArticleService.getEditArticleData()));

        $location.path('/editArticle');
    }

    $scope.to_trusted = function(html_code) {
        return $sce.trustAsHtml(html_code);
    }


    $scope.initTextEditor = function() {
        $("#textEditor").summernote({
            height: 300
        });
        console.log("Text editor initialized");
    }

    $scope.initEditTextEditor = function() {
        console.log("edit texteditor initializing...")
        $("#textEditor").summernote({
            height: 300
        });
        console.log("Text editor initialized");
        $("#textEditor").code(ArticleService.getEditArticleData().content);
        console.log("Text editor content filled");
        $scope.newTitle = ArticleService.getEditArticleData().title;
        $scope.editArticleId = ArticleService.getEditArticleData().id;
    }
});
