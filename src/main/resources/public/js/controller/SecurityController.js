angular.module('blogModule.SecurityController', []).

controller('SecurityController' , function($scope, SecurityService) {

    $scope.isLoggedIn = false;

    $scope.checkStatus = function checkStatus() {
        SecurityService.status().
            then(function(response) {
                if(response.data.result == true) {
                    $scope.isLoggedIn = true;
                } else {
                    $scope.isLoggedIn = false;
                }
                console.log("AJAX call finished. Status=" + response.data.result);
            });
    }

    $scope.logout = function() {
        SecurityService.logout().
            then(function(response) {
                if(response.data.status == "OK") {
                    $scope.isLoggedIn = false;
                }
            });
    }

    // TODO : Not working ATM
    $scope.securityInit = function() {
        console.log("SecurityController Initializing....");
        $scope.checkStatus();
        console.log("LoginStatus=" + $scope.isLoggedIn);
    }


    // TODO : Too much redundant code....
    $scope.login = function(input) {

        $('#login-button').button('loading')
        console.log(JSON.stringify(input));

        if(input == undefined) {
            $('#login-button').button('reset')
            return;
        }

        SecurityService.login(input.username, input.password).
            then(function (response){

                if(response.data.status == "ERROR") {

                    $('#loginErrorMessageDiv').removeClass('hidden')
                    $('#loginErrorMessageSpan').text(response.data.message);

                    $scope.input.password = "";

                    $('#login-button').button('reset')

                } else if(response.data.status == "OK") {

                    $('#myModal').modal('hide');
                    $('#login-button').button('reset')

                    $scope.isLoggedIn = true;

                }
            }, function(response) {
                // None OK response
                // happens if arguments doesn't meet java's constraints

                $('#loginErrorMessageDiv').removeClass('hidden')
                $('#loginErrorMessageSpan').text("Login failed. Check your credentials");

                // TODO : This is hardcoded message
                // TODO : Proper way is change the back-end to not send this status code
                $('.ui.ok.button').removeClass("loading");
                $('#login-button').button('reset')
            });

    }

    $scope.loginButtonClick = function() {

        $scope.input = undefined;
        $('#loginErrorMessageDiv').addClass('hidden')
        $('#myModal').modal('show');
    }




});

