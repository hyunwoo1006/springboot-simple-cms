angular.module('blogModule.ArticleService', []).

factory('ArticleService', function($http) {
    var factory = {};

    var editArticleData;

    factory.setEditArticleData = function(article) {
        editArticleData = article;
    }

    factory.getEditArticleData = function() {
        return editArticleData;
    }

    factory.getArticles = function() {

        return $http.get(apiUrl + '/article');
    }

    factory.saveComment = function(articleId, content) {

        var payload = {};
        payload["content"] = content;

        return $http.post(apiUrl + '/article/' + articleId +'/comment', payload);

//        alert("inside article service");
    }

    factory.deleteArticle = function(articleId) {
        return $http.delete(apiUrl + '/article/' + articleId);
    }

    factory.editArticle = function(id, title, content) {

        var payload = {};
        payload["id"] = id;
        payload["title"] = title;
        payload["content"] = content;

        return $http.put(apiUrl + '/article', payload);
    }

    factory.createArticle = function(title, content) {

        var payload = {};
        payload["title"] = title;
        payload["content"] = content;

        return $http.post(apiUrl + '/article', payload);

    }

    return factory;
});