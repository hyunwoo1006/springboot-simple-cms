angular.module('blogModule.SecurityService', []).

factory('SecurityService', function($http) {
    var factory = {};

    factory.login = function(username, password) {

        var payload = {};
        payload["username"] = username;
        payload["password"] = password;

        return $http.post(apiUrl + '/security/login', payload);
    }


    factory.logout = function() {
        return $http.post(apiUrl + '/security/logout', null);
    }

    factory.status = function() {
        return $http.get(apiUrl + '/security/status');
    }

    return factory;
});