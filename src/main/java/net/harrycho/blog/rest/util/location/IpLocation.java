package net.harrycho.blog.rest.util.location;

public class IpLocation {
    private String countryCode = null;
    private String countryName = null;
    private String city = null;
    private String ipAddress = null;

    public IpLocation(String countryCode, String countryName, String city, String ipAddress) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.city = city;
        this.ipAddress = ipAddress;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCity() {
        return city;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IpLocation{");
        sb.append("countryCode='").append(countryCode).append('\'');
        sb.append(", countryName='").append(countryName).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", ipAddress='").append(ipAddress).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
