package net.harrycho.blog.rest.util.location;


import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

public final class IpLocationUtil {

    private static final Logger log = LogManager.getLogger(IpLocationUtil.class);

    private static final String ipLocationDbPath = "locationdb/GeoLiteCity.dat";

    private IpLocationUtil() {}

    private static LookupService lookupService = null;

    static {
        try {

            log.debug("IpLocationUtil initializing... DbFilePath=" + ipLocationDbPath);
            InputStream istream = IpLocationUtil.class.getClassLoader().getResourceAsStream("locationdb/GeoLiteCity.dat");
            log.debug("istream size=" + istream.available());

            File dbFile = new File("ipLocationDbFile");
            FileUtils.copyInputStreamToFile(istream, dbFile);
            log.debug("File copied");

            lookupService = new LookupService(dbFile, LookupService.GEOIP_MEMORY_CACHE);
        } catch (IOException e) {
            log.error("Error while initalizing IpLocationUtil", e);
        }
    }

    public static IpLocation getLocation(InetAddress ipAddress) {
        if(lookupService == null) {
            log.error("Unable to get location. Lookup servie is null. ipLocationDbPath=" + ipLocationDbPath);
            return null;
        }
        Location location = lookupService.getLocation(ipAddress);
        return new IpLocation(location.countryCode, location.countryName, location.city, ipAddress.toString());
    }

    public static IpLocation getLocation(String ipAddress) {
        if (lookupService == null) {
            log.error("Unable to get location. Lookup servie is null. ipLocationDbPath=" + ipLocationDbPath);
            return null;
        }
        Location location = lookupService.getLocation(ipAddress);
        if(location == null) {
            log.error("Lookup service did not returned any location for ip=" + ipAddress);
            return null;
        }
        log.info("Access from location " + location.toString());
        return new IpLocation(location.countryCode, location.countryName, location.city, ipAddress.toString());
    }



}
