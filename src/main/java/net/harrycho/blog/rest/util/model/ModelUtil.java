package net.harrycho.blog.rest.util.model;

import com.maxmind.geoip.Location;
import net.harrycho.blog.rest.model.BaseModel;
import net.harrycho.blog.rest.util.location.IpLocation;
import net.harrycho.blog.rest.util.location.IpLocationUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;

import java.security.Security;

public class ModelUtil {

    private static final Logger log = LogManager.getLogger(ModelUtil.class);

    public static void setFieldsForNewlyCreated(BaseModel model) {
        model.setCreated(System.currentTimeMillis());
        model.setUpdated(System.currentTimeMillis());

        if(SecurityUtils.getSubject().getPrincipal() == null) {

//            Set only the hostname
            String host = SecurityUtils.getSubject().getSession().getHost();

            IpLocation location = IpLocationUtil.getLocation(host);

            String countryName = "";

            if(location == null || location.getCountryName() == null) {
                log.info("location is null or country name is null. location=" + location);
                countryName = host;
            } else {
                countryName = location.getCountryName();
            }

            model.setCreatedBy("Anonymous user from " + countryName);

//            model.setCreatedBy(SecurityUtils.getSubject().getSession().getHost() +
//                ":" + SecurityUtils.getSubject().getSession().getId());
        } else {
            model.setCreatedBy(SecurityUtils.getSubject().getPrincipal().toString());
        }
    }

    public static void updateTimestamp(BaseModel saved, BaseModel update) {
        update.setCreated(saved.getCreated());
        update.setCreatedBy(saved.getCreatedBy());
        update.setUpdated(System.currentTimeMillis());
    }

}
