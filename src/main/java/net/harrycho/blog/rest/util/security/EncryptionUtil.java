package net.harrycho.blog.rest.util.security;

import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Component
public class EncryptionUtil {

    private static SecureRandom random = new SecureRandom();

    public static String hashPassword(String pass, String salt) {
        return new Sha512Hash(pass, salt, 5).toBase64();
    }

    public static String getRandomSalt() {
        return new BigInteger(130, random).toString();
    }

}
