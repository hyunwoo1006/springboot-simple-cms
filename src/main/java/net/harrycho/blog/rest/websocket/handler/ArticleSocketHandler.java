package net.harrycho.blog.rest.websocket.handler;

import net.harrycho.blog.rest.websocket.ArticleBroadcastingService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

public class ArticleSocketHandler extends TextWebSocketHandler {
    private static final Logger log = LogManager.getLogger(ArticleSocketHandler.class);

    @Autowired
    ArticleBroadcastingService broadcastingService;

    public ArticleSocketHandler() {
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException, InterruptedException {
        log.debug("session message : " + session.getId());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        log.debug("websocket opened...");
        broadcastingService.subscribe(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        log.debug("websocket closed...");
        broadcastingService.unsubscribe(session);
    }
}
