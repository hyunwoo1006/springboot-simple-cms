package net.harrycho.blog.rest.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.harrycho.blog.rest.model.Comment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ArticleBroadcastingService{

    private static final Logger log = LogManager.getLogger(ArticleBroadcastingService.class);

    private Map<String, WebSocketSession> sessionMap;
    private ObjectMapper objectMapper;

    public ArticleBroadcastingService() {
        sessionMap = new ConcurrentHashMap<String, WebSocketSession>();
        objectMapper = new ObjectMapper();
    }


    public void notifyCommentChange(long id, Comment comment) {
        log.debug("Notify comment change trigged.. notifying subcribers..");
        for(WebSocketSession session : sessionMap.values()) {
            try {
                Map<String, Object> data = new HashMap<String, Object>();
                data.put("articleId", id);
                data.put("comment", comment);
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(data)));
            } catch (IOException e) {
                log.error("Exception occurrerd while sending comment thorugh websocket. Comment=" +
                        comment.toString(), e);
            }
        }
    }

    public void subscribe(WebSocketSession session) {
        sessionMap.put(session.getId(), session);

    }

    public void unsubscribe(WebSocketSession session) {
        sessionMap.remove(session.getId());

    }

}
