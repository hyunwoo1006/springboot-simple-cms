package net.harrycho.blog.rest.websocket.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EchoClockService extends Thread{

    private static final Logger log = LogManager.getLogger(EchoClockService.class);

    private Map<String, WebSocketSession> sessionMap;

    private boolean sessionExpiredTimerRunning;
    private boolean echoClockRunning;

    private int interval;

    private SimpleDateFormat format;

    public EchoClockService(int interval) {

        format = new SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy");

        this.interval = interval;
        sessionExpiredTimerRunning = true;
        echoClockRunning = true;

        sessionMap = new ConcurrentHashMap<String, WebSocketSession>();

//        SessionExpiredTimer expiredTimer = new SessionExpiredTimer();
//        expiredTimer.start();

    }


    @Override
    public void run() {
        while(echoClockRunning) {

            for(WebSocketSession session : sessionMap.values()) {
                try {
                    session.sendMessage(new TextMessage("Current system time is : "  + format.format(new Date())));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    private void stopSessionExpiredTimer() {
        sessionExpiredTimerRunning = false;
    }

    public void subscribe(WebSocketSession session) {
        sessionMap.put(session.getId(), session);
    }

    public void unsubscribe(WebSocketSession session) {
        sessionMap.remove(session.getId());
    }

    public void stopEchoClock() {

        stopSessionExpiredTimer();
        sessionMap.clear();

    }

//    private class SessionExpiredTimer extends Thread{
//
//        @Override
//        public void run() {
//            while(sessionExpiredTimerRunning) {
//                log.debug("ExpiredTimer Check. Number of session in cache:" + sessionMap.size());
////                for (WebSocketSession session : sessionMap.values()) {
////                    if(!session.isOpen()) {
////                        sessionMap.remove(session);
////                        log.debug("Seesion:" + session.getId() + " is closed. Removed. Number of sessions remaining in cache:" + sessionMap.size());
////                    }
////                }
//
//                try {
//                    Thread.sleep(5000L);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
}
