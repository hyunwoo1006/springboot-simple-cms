package net.harrycho.blog.rest.websocket.handler;

import net.harrycho.blog.rest.controller.ArticleController;
import net.harrycho.blog.rest.websocket.test.EchoClockService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

public class WebSocketTestHandler extends TextWebSocketHandler {

    private static final Logger log = LogManager.getLogger(WebSocketTestHandler.class);

    private EchoClockService echoClockService;

    public WebSocketTestHandler() {
        echoClockService = new EchoClockService(1000);
        echoClockService.start();
    }

    @Autowired
    private ArticleController articleController;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException, InterruptedException {
        log.debug("I'm here !! This is the message I got : " + message.getPayload());
        session.sendMessage(new TextMessage("YOYO I GOT YOUR MESSAGE : " + message.getPayload()));
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        echoClockService.subscribe(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        echoClockService.unsubscribe(session);
    }
}
