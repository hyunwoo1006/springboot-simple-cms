package net.harrycho.blog.rest.service.article;

import net.harrycho.blog.rest.model.Article;
import net.harrycho.blog.rest.model.Comment;
import net.harrycho.blog.rest.database.ArticleDB;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

@Component
public class ArticleService {


    private static final Logger log = LogManager.getLogger(ArticleService.class);

    @Autowired
    private ArticleDB articleDB;

    public Article getArticle(long id) {
        return articleDB.getArticle(id);
    }

    public List<Article> getAllArticles() {
        return articleDB.getArticles();

    }

    public boolean deleteArticle(long id) {

        Article savedArticle = getArticle(id);

        if(savedArticle == null) {
            return false;
        }

        articleDB.deleteArticle(id);
        return true;
    }

    public void createArticle(Article article) {
        log.debug("Saving article : " + article);
        article.setTitle(HtmlUtils.htmlEscape(article.getTitle()));
        articleDB.saveArticle(article);
    }

    public boolean updateArticle(Article updateArticle) {
        log.debug("Updating article : " + updateArticle);

        Article savedArticle = getArticle(updateArticle.getId());
        if(savedArticle == null) {
            log.info("Updated failed due to no article found by id. Id:" + updateArticle.getId());
            return false;
        }

        articleDB.updateArticle(savedArticle, updateArticle);
        return true;
    }

    public boolean createComment(Comment comment) {
        log.debug("Saving comment : " + comment);

        Article savedArticle = getArticle(comment.getArticle().getId());
        if(savedArticle == null) {
            log.info("Creating comment failed due to no article found by id. Id:" + comment.getArticle().getId());
            return false;
        }

        // Escape content
        comment.setContent(HtmlUtils.htmlEscape(comment.getContent()));

        articleDB.saveComment(comment);
        return true;
    }

    public Comment getComment(long id) {
        return articleDB.getComment(id);
    }

    public boolean deleteComment(long id) {

        Comment savedComment = getComment(id);

        if(savedComment == null) {
            return false;
        }

        articleDB.deleteComment(id);
        return true;
    }
}
