package net.harrycho.blog.rest.service.security;

public final class SecurityConstants {
    private SecurityConstants() {
        // Empty Constructor
    }

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 30;
    public static final int USERNAME_MIN_LENGTH = 4;
    public static final int USERNAME_MAX_LENGTH = 40;

}
