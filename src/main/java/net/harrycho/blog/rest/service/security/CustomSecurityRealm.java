package net.harrycho.blog.rest.service.security;


import net.harrycho.blog.rest.model.User;
import net.harrycho.blog.rest.util.security.EncryptionUtil;
import net.harrycho.blog.rest.database.SecurityDB;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class CustomSecurityRealm extends AuthorizingRealm {


    private static final Logger log = LogManager.getLogger(CustomSecurityRealm.class);

    @Autowired
    private SecurityDB securityDB;

    @Override
    public String getName() {
        return CustomSecurityRealm.class.getName();
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        AuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        return authorizationInfo;
    }

    /**
     *
     * AuthenticationToken is the one user provided. It is an instance of UsernamePasswordToken.
     * AuthenticationInfo is the expected user info which contains principal and expected credentials.
     *
     *  SimpleCredentialMatcher simply does equals operations on two credentials (strings).
     *  // TODO : Replace SimpleCredentialMatcher with HashedCredentialMatcher
     *
     * Token's password is changed by the hashed password.
     *
     *
     *
     * Basic workflow is like this
     *  1. User provided credentials received
     *  2. With username, get the user stored in the database.
     *     a. if user is not found, return null
     *  3. Given user from database, get the salt and hash the user provided credential and replace provided credential
     *  4. Given user from database, get the username (principal) and make a simple principal collection
     *  5. Create SimpleAuthenticationInfo using principal and expected credentials
     *
     *
     *
     *
     *
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
            throws AuthenticationException {

        UsernamePasswordToken  usernamePasswordToken = (UsernamePasswordToken) token;

        User user = securityDB.getUserByUsername(usernamePasswordToken.getUsername());

        // User does not exist
        if(user == null) {
           throw new UnknownAccountException("Accout not found for provided username: "
                            + usernamePasswordToken.getUsername());
        }

        // TODO : Salted and Hashing should not be done at this stage. May be authenticator?
        String salt = user.getSalt();
        String enteredPassword = new String(usernamePasswordToken.getPassword());
        String hashedPassword = EncryptionUtil.hashPassword(enteredPassword, salt);

        log.printf(Level.DEBUG,
                        "\n\tSalt:\'%s\'\n" +
                        "\tSupplied:\'%s\'\n" +
                        "\tSuppliedHashed:\t\'%s\'\n" +
                        "\tSavedHashed:\t\'%s\'",
                salt, enteredPassword, hashedPassword, user.getHashedPassword());

        usernamePasswordToken.setPassword(hashedPassword.toCharArray());

        // First principle is what you get from subject.getPrinciple();
        Collection<Object> principals = new ArrayList<Object>();
        principals.add(user.getUsername());
        SimplePrincipalCollection principalCollection = new SimplePrincipalCollection(principals, getName());
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(principalCollection, user.getHashedPassword());

        return authenticationInfo;
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        if(token instanceof UsernamePasswordToken == false) {
            return false;
        }
        return true;
    }
}
