package net.harrycho.blog.rest.service.security;


import net.harrycho.blog.rest.model.User;
import net.harrycho.blog.rest.model.UserForm;
import net.harrycho.blog.rest.util.security.EncryptionUtil;
import net.harrycho.blog.rest.database.SecurityDB;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Validator;

import java.util.List;


@Component
public class SecurityService {


    private static final Logger log = LogManager.getLogger(SecurityService.class);

    @Autowired
    private Validator validator;

    @Autowired
    private SecurityDB securityDB;


    public void logout() {
        log.trace("Attempting to logout");
        SecurityUtils.getSubject().logout();
    }

    public boolean login(UserForm credential) {

        log.trace("Attempting logging in with credential:" + credential);

        UsernamePasswordToken token = new UsernamePasswordToken(credential.getUsername(), credential.getPassword());
        Subject currentUser = SecurityUtils.getSubject();

        try {
            currentUser.login(token);
        } catch (UnknownAccountException e) {
            log.trace("Account not found. Token: " + token, e);
            return false;
        } catch (IncorrectCredentialsException e ) {
            log.trace("Account not found. Token: " + token, e);
            return false;
        }

        return true;

    }

    public boolean saveUser(UserForm userInfo) {

        // User exists return false;
        if(securityDB.getUserByUsername(userInfo.getUsername()) != null){
            return false;
        }

        String salt = EncryptionUtil.getRandomSalt();

        String hashedPassword = EncryptionUtil.hashPassword(userInfo.getPassword(), salt);

        User user = new User();
        user.setSalt(salt);
        user.setHashedPassword(hashedPassword);
        user.setUsername(userInfo.getUsername());

        log.printf(Level.DEBUG, "\n\tEntered:\'%s\'\n\tSalt:\'%s\'\n\tHashedWithSalt:\'%s\'",
                userInfo.getPassword(), salt, hashedPassword);

        securityDB.saveUser(user);

        return true;
    }


    public List<User> getUsers() {
        return securityDB.getAllUsers();
    }


    public boolean deleteUser(UserForm user) {

        User savedUser = securityDB.getUserByUsername(user.getUsername());

        if(savedUser != null) {

            if(SecurityUtils.getSubject().getPrincipal().equals("administrator")) {
                securityDB.deleteUserByUsername(user.getUsername());
                log.info("User deleted (admin).");
            } else {
                if(doesPasswordMatch(user.getPassword(), savedUser.getSalt(), savedUser.getHashedPassword()) == true)  {
                    securityDB.deleteUserByUsername(user.getUsername());
                    log.info("User deleted (non-admin).");
                } else {
                    log.error("Attempted to delete user but pass not match. Inputted form:" + user);
                    return false;
                }
            }
            return true;

        } else {
            log.error("Attempted to delete user but user not found. Username:" + user.getUsername());
            return false;
        }

    }

    private boolean doesPasswordMatch(String inputPassword, String salt, String savedHashedPassword) {

       return savedHashedPassword.equals(EncryptionUtil.hashPassword(inputPassword, salt));
    }
}


