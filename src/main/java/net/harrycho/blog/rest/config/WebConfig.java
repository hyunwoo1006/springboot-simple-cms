package net.harrycho.blog.rest.config;

import net.harrycho.blog.rest.service.security.CustomSecurityRealm;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.session.mgt.ExecutorServiceSessionValidationScheduler;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.env.EnvironmentLoaderListener;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.ShiroFilter;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.servlet.Filter;
import java.util.*;

/**
 * Replacement for shiro.ini(not completely..) and web.xml
 *
 *
 *
 *
 */
@Configuration
public class WebConfig {

    /**
     * Shiro specific.
     *
     * In web.xml, this is listener-class tag
     *
     * <listener>
     *     <listener-class>org.apache.shiro.web.env.EnvironmentLoaderListener</listener-class>
     * </listener>
     * @return
     */
    @Bean
    public EnvironmentLoaderListener environmentLoaderListener() {
        EnvironmentLoaderListener listener = new EnvironmentLoaderListener();
        return listener;
    }


    /**
     * SpringBoot specific. No longer uses web.xml
     *
     * In web.xml, this is listner tag
     *
     * <listener>
     *     <listener-class>org.apache.shiro.web.env.EnvironmentLoaderListener</listener-class>
     * </listener>
     * @return
     */
    @Bean
    public ServletListenerRegistrationBean servletRegistrationBean() {
        ServletListenerRegistrationBean registrationBean = new ServletListenerRegistrationBean();
        registrationBean.setListener(environmentLoaderListener());
        return registrationBean;
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean() {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager());
        Map<String, Filter> filters = new HashMap<String, Filter>();

        filters.put("shiroFilter", new ShiroFilter());
        shiroFilterFactoryBean.setFilters(filters);
        return shiroFilterFactoryBean;
    }


    /**
     * Needed for Shiro annotations (http://shiro.apache.org/spring.html)
     *
     *
     *
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor =
                new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
        return authorizationAttributeSourceAdvisor;
    }


    /**
     * Shiro security manager.
     *
     *
     * Registers Realm bean, SessionManager bean, CacheManager bean.
     *
     *
     * @return
     */
    @Bean
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(customSecurityRealm());
        securityManager.setSessionManager(defaultWebSessionManager());
        securityManager.setCacheManager(memoryConstrainedCacheManager());
        return securityManager;
    }

    /**
     * CustomeSecurityRealm bean. Used by Shiro's SecurityManager bean.
     *
     *
     * @return
     */
    @Bean
    public CustomSecurityRealm customSecurityRealm() {
        return new CustomSecurityRealm();
    }

    /**
     * Shiro session manager bean.
     *
     *
     * @return
     */
    @Bean
    public DefaultWebSessionManager defaultWebSessionManager() {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionValidationScheduler(sessionValidationScheduler());
        sessionManager.setSessionIdCookieEnabled(true);
        return sessionManager;
    }

    @Bean
    public ExecutorServiceSessionValidationScheduler sessionValidationScheduler() {
        ExecutorServiceSessionValidationScheduler sessionValidationScheduler =
                new ExecutorServiceSessionValidationScheduler();
        sessionValidationScheduler.setInterval(3600000);
        return sessionValidationScheduler;
    }


    /**
     * Shiro CacheManager bean.
     *
     *
     *
     * @return
     */
    @Bean
    public MemoryConstrainedCacheManager memoryConstrainedCacheManager() {
        return new MemoryConstrainedCacheManager();
    }


    /**
     * Needed for Shiro annotations. (http://shiro.apache.org/spring.html)
     *
     * @return
     */
    @Bean
    @DependsOn(value = "lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        return new DefaultAdvisorAutoProxyCreator();
    }



    /**
     * Shiro specific. Need for using shiro beans.
     *
     * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }


    /**
     * Needed for validation
     *
     *
     * @return
     */
    @Bean(name = "validator")
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }


}
