package net.harrycho.blog.rest.config;

import net.harrycho.blog.rest.websocket.handler.ArticleSocketHandler;
import net.harrycho.blog.rest.websocket.handler.WebSocketTestHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


/**
 * WebSocket Configuration.
 *
 *  Place to register websocket handlers.
 *
 *
 *
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//        registry.addHandler(testHandler(), "ws/test").setAllowedOrigins("*");
        registry.addHandler(articleHandler(), "ws/article").setAllowedOrigins("*");
    }

    @Bean
    public WebSocketHandler testHandler() {
        return new WebSocketTestHandler();
    }

    @Bean
    public WebSocketHandler articleHandler() {
        return new ArticleSocketHandler();
    }
}
