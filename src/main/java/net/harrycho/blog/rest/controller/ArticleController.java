package net.harrycho.blog.rest.controller;

import net.harrycho.blog.rest.model.Article;
import net.harrycho.blog.rest.model.Comment;
import net.harrycho.blog.rest.model.response.ResultResponse;
import net.harrycho.blog.rest.service.article.ArticleService;
import net.harrycho.blog.rest.model.response.Response;
import net.harrycho.blog.rest.websocket.ArticleBroadcastingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static net.harrycho.blog.rest.model.response.ResponseConstants.*;

import javax.validation.Valid;
import java.util.List;



@Controller
public class ArticleController {

    private static final Logger log = LogManager.getLogger(ArticleController.class);

    @Autowired
    private ArticleBroadcastingService broadcastingService;

    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "article/{id}", method = RequestMethod.GET)
    public @ResponseBody Response getArticle(@PathVariable long id) {

        log.debug("Getting article id=" + id);
        Article article = articleService.getArticle(id);

        return new ResultResponse(article);

    }

    @RequestMapping(value = "article",  method = RequestMethod.GET)
    public @ResponseBody Response getArticles() {
        log.info("Retrieving all articles");

        List<Article> articleList = articleService.getAllArticles();

        log.debug("All articles=" + articleList);

        return new ResultResponse(articleList);

    }

    @RequiresAuthentication
    @RequestMapping(value = "article/{id}", method = RequestMethod.DELETE)
    public @ResponseBody  Response deleteArticle(@PathVariable long id) {

        // TODO : Do not delete the item. Instead, set delete flag to true in DB

        boolean status = articleService.deleteArticle(id);
        if(status == false) {
            return new Response(STATUS_ERROR);
        }

        return new Response(STATUS_OK);

    }

    @RequiresAuthentication
    @RequestMapping(value = "article", method = RequestMethod.PUT)
    public @ResponseBody Response updateArticle(@RequestBody @Valid Article article) {

        boolean updateStatus = articleService.updateArticle(article);

        if(updateStatus == false) {
            new Response(STATUS_ERROR);
        }

        return new Response(STATUS_OK);
    }

    /**
     * Auto-generated field gets ignored and always creates a new article
     *
     * @param article
     * @return
     */
    @RequiresAuthentication
    @RequestMapping(value = "article", method=RequestMethod.POST)
    public @ResponseBody Response createArticle(@RequestBody @Valid Article article) {

        articleService.createArticle(article);

        return new Response(STATUS_OK);

    }


    /**
     * Always creates a new comment to an article
     *
     * @param id
     * @param comment
     * @return
     */
    @RequestMapping(value = "article/{id}/comment", method = RequestMethod.POST)
    public @ResponseBody Response createComment(@PathVariable long id, @RequestBody @Valid Comment comment) {
        log.debug("id:" + id + ", comment:" + comment);

        comment.setArticle(new Article(id));

        boolean status = articleService.createComment(comment);
        if(status == false) {
            return new Response(STATUS_ERROR);
        }

        log.debug("Saved comment:" + comment.toString());

        // Notify all subscribed websockets that a comment is added
        log.debug("Notifying comment created...");
        broadcastingService.notifyCommentChange(id, comment);
        return new Response(STATUS_OK);
    }

    @RequiresAuthentication
    @RequestMapping(value = "article/comment/{id}", method = RequestMethod.DELETE)
    public @ResponseBody Response deleteComment(@PathVariable long id) {
        log.debug("deleting comment id: " + id);

        boolean status = articleService.deleteComment(id);
        if(status == false) {
            return new Response(STATUS_ERROR);
        }
        return new Response(STATUS_OK);
    }

}
