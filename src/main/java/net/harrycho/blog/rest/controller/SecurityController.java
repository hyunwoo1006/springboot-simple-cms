package net.harrycho.blog.rest.controller;

import net.harrycho.blog.rest.model.User;
import net.harrycho.blog.rest.model.UserForm;
import net.harrycho.blog.rest.model.response.ErrorMessageResponse;
import net.harrycho.blog.rest.model.response.ResultResponse;
import net.harrycho.blog.rest.service.security.SecurityService;
import net.harrycho.blog.rest.model.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;

import java.util.List;
import java.util.Set;

import static net.harrycho.blog.rest.model.response.ResponseConstants.STATUS_OK;


@Controller
public class SecurityController {

    private static final Logger log = LogManager.getLogger(SecurityController.class);

    @Autowired
    private SecurityService securityService;

    @Autowired
    private Validator validator;

    /**
     *
     *
     * @param credential
     * @return
     */
    @RequestMapping(value = "/security/login", method = RequestMethod.POST)
    public @ResponseBody Response login(@RequestBody @Valid UserForm credential) {

        log.trace("Credential received:" + credential);
        log.trace("Cookie ID : " + SecurityUtils.getSubject().getSession().getId());


        boolean status = securityService.login(credential);
        log.debug("login status : " + status);

        if(status == false) {
            // If user is logged in (authenticated) and another login request was made with invalid
            // credential, then force the user to logout.
            SecurityUtils.getSubject().logout();
            return new ErrorMessageResponse("Login failed. Check your credentials");
        } else {
            return new Response(STATUS_OK);
        }

    }

    /**
     *
     * Logout from current user;
     *
     * TODO : Is this all I really have to do? Think about security issues.
     *
     *
     * @return
     */
    @RequestMapping(value = "/security/logout", method = RequestMethod.POST)
    public @ResponseBody Response logout() {
        securityService.logout();
        return new Response(STATUS_OK);
    }

    @RequestMapping(value = "/security/status", method = RequestMethod.GET)
    public @ResponseBody Response status() {
        if(SecurityUtils.getSubject().isAuthenticated() == true) {
            return new ResultResponse(true);
        }
        return new ResultResponse(false);

    }

    @RequiresAuthentication
    @RequestMapping(value = "/security/user", method = RequestMethod.POST)
    public @ResponseBody Response createUser(@RequestBody UserForm user) {

        Set<ConstraintViolation<UserForm>> violations = validator.validate(user);

        // Username is saved as lower case
        user.setUsername(user.getUsername().toLowerCase());

        if(violations.isEmpty() == false) {
            // There are some error for user inputted form
            // TODO : Should give error codes. Need to define library of error codes and messages
            return new ErrorMessageResponse(violations.toString());
        }

        boolean status = securityService.saveUser(user);
        if(status == false) {
            return new ErrorMessageResponse("account already exists");
        }

        return new Response(STATUS_OK);
    }


    @RequiresAuthentication
    @RequestMapping(value = "/security/user", method = RequestMethod.GET)
    public @ResponseBody Response getUsers() {
        List<User> users = securityService.getUsers();

        return new ResultResponse(users);
    }


    /**
     * TODO : Implement deleting user
     *
     * @param user
     * @return
     */
    @RequiresAuthentication
    @RequestMapping(value = "security/user", method = RequestMethod.DELETE)
    public @ResponseBody Response deleteUser(@RequestBody @Valid UserForm user) {

        if(user.getUsername().equals("administrator")) {
            return new ErrorMessageResponse("Cannot delete administrator");
        }


        boolean status = securityService.deleteUser(user);
        if(status == false) {
            return new ErrorMessageResponse("Delete Failed");
        }
        return new Response(STATUS_OK);
    }


    // TODO : Password change



}
