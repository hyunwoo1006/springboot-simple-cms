package net.harrycho.blog.rest.model;

import javax.persistence.*;

@MappedSuperclass
public abstract class BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    protected long id;

    @Column(name = "created", nullable = false)
    protected long created;

    @Column(name = "updated", nullable = false)
    protected long updated;

    @Column(name = "createdBy", nullable = false)
    protected String createdBy;



    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseModel baseModel = (BaseModel) o;

        if (created != baseModel.created) return false;
        if (id != baseModel.id) return false;
        if (updated != baseModel.updated) return false;
        if (createdBy != null ? !createdBy.equals(baseModel.createdBy) : baseModel.createdBy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (created ^ (created >>> 32));
        result = 31 * result + (int) (updated ^ (updated >>> 32));
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "created=" + created +
                ", updated=" + updated +
                ", createdBy='" + createdBy + '\'' +
                ", id=" + id +
                '}';
    }
}
