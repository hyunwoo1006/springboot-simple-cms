package net.harrycho.blog.rest.model;

import javax.persistence.*;

@Entity
@Table(
        name = "user",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "username"})}
)
public class User extends BaseModel {


    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "hashedPassword", nullable = false)
    private String hashedPassword;

    @Column(name = "salt", nullable = false)
    private String salt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

}
