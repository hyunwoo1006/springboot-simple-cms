package net.harrycho.blog.rest.model.response;

public class ErrorMessageResponse extends Response{

    private String message;

    public ErrorMessageResponse(String message) {
        super(ResponseConstants.STATUS_ERROR);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
