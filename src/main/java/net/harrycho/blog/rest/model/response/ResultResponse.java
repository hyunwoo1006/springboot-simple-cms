package net.harrycho.blog.rest.model.response;

public class ResultResponse extends Response{

    private Object result;

    public ResultResponse(Object result) {
        super(ResponseConstants.STATUS_OK);
        this.result = result;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
