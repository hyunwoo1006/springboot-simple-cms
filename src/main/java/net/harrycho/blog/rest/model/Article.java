package net.harrycho.blog.rest.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Set;

@Entity
@Table(
        name = "article",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})}
)
public class Article extends BaseModel{

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(mappedBy = "article")
    private Set<Comment> comments;


    public Article() {
    }

    public Article(long id) {
        setId(id);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
