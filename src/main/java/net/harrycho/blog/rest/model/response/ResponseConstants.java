package net.harrycho.blog.rest.model.response;

public final class ResponseConstants {

    private ResponseConstants() {
    }

    public static final String STATUS_OK = "OK";
    public static final String STATUS_ERROR = "ERROR";

}
