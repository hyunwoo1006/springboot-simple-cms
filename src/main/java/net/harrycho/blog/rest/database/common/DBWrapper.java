package net.harrycho.blog.rest.database.common;

import net.harrycho.blog.rest.model.BaseModel;
import net.harrycho.blog.rest.util.model.ModelUtil;
import net.harrycho.blog.rest.util.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class DBWrapper {

    public void save(BaseModel o) {

        BaseModel model = o;
        ModelUtil.setFieldsForNewlyCreated(model);

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(model);
        session.getTransaction().commit();
        session.close();
    }

    public void update(Object o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(Object o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        session.close();
    }

    public Session startSession() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        return session;
    }

    public void finishCommitSession(Session session) {
        session.getTransaction().commit();
        session.close();
    }

    public void finishSession(Session session) {
        session.close();
    }


}
