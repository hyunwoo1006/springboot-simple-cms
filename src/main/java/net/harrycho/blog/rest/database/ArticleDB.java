package net.harrycho.blog.rest.database;

import net.harrycho.blog.rest.model.Article;
import net.harrycho.blog.rest.model.Comment;
import net.harrycho.blog.rest.util.model.ModelUtil;
import net.harrycho.blog.rest.database.common.DBWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ArticleDB {


    private static final Logger logger = LogManager.getLogger(ArticleDB.class);

    private static final String DELETE_ARTICLE_BY_ID_QUERY =
            "DELETE FROM Article " +
            "WHERE id = :id";

    private static final String GET_COMMENT_BY_ID_QUERY =
            "FROM Comment " +
            "WHERE id = :id";

    private static final String DELETE_COMMENT_BY_ID_QUERY =
            "DELETE FROM Comment " +
            "WHERE id = :id";

    private static final String GET_ARTICLE_BY_ID_QUERY =
            "FROM Article " +
            "WHERE id = :id";

    private static final String GET_ARTICLES_QUERY =
            "FROM Article";


    @Autowired
    private DBWrapper dbWrapper;

    public void saveArticle(Article article) {
        ModelUtil.setFieldsForNewlyCreated(article);
        dbWrapper.save(article);
    }


    public void updateArticle(Article savedArticle, Article updateArticle) {

        ModelUtil.updateTimestamp(savedArticle, updateArticle);
        dbWrapper.update(updateArticle);
    }


    /**
     * Deletes an article specified by the ID. If more than 1 items were modified as a result of
     *  query, it logs the error.
     *
     * @param id    ID of an Article
     *
     * @return      returns 1 if deleted, 0 if not found.
     */
    public void deleteArticle(long id) {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(DELETE_ARTICLE_BY_ID_QUERY);
        query.setParameter("id", id);
        int i = query.executeUpdate();
        if(i > 1) {
            logger.error("More than 1 item has been modified by delete. Items modified:" + i);
        } else if( i == 0 ){
            logger.error("Nothing has been deleted. This error checking should have been done at service level. Id:" + id);
        }
        dbWrapper.finishCommitSession(session);
    }

    /**
     * Gets an article specified by the ID. It executes the query and returns the first item in the query.
     *  If more than 1 item was found, it logs the error.
     *
     * @param id    ID of an Article
     *
     * @return      returns Article if found, otherwise returns null
     */
    public Article getArticle(long id) {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(GET_ARTICLE_BY_ID_QUERY);
        query.setParameter("id", id);
        List<Article> queryResult = query.list();
        if(queryResult.isEmpty()) {
            return null;
        } else if(queryResult.size() > 1) {
            logger.error("There were more than 1 with the same id. QueryResults:" + queryResult);
        }
        dbWrapper.finishSession(session);
        return queryResult.get(0);
    }


    /**
     * Gets all articles and returns in a list
     *
     * @return List of all articles
     */
    public List<Article> getArticles() {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(GET_ARTICLES_QUERY);
        List queryResult = query.list();
        dbWrapper.finishSession(session);
        return queryResult;
    }

    public Comment getComment(long id) {

        Session session = dbWrapper.startSession();
        Query query = session.createQuery(GET_COMMENT_BY_ID_QUERY);
        query.setParameter("id", id);
        List<Comment> queryResult = query.list();
        if(queryResult.isEmpty()) {
            return null;
        } else if(queryResult.size() > 1) {
            logger.error("There were more than 1 with the same id. QueryResults:" + queryResult);
        }
        dbWrapper.finishSession(session);
        return queryResult.get(0);
    }

    public void saveComment(Comment comment) {
        dbWrapper.save(comment);
    }

    public void deleteComment(long id) {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(DELETE_COMMENT_BY_ID_QUERY);
        query.setParameter("id", id);
        int i = query.executeUpdate();
        if(i > 1) {
            logger.error("More than 1 item has been modified by delete. Items modified:" + i);
        }
        dbWrapper.finishCommitSession(session);
    }


}
