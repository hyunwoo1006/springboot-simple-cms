package net.harrycho.blog.rest.database;


import net.harrycho.blog.rest.model.User;
import net.harrycho.blog.rest.util.model.ModelUtil;
import net.harrycho.blog.rest.database.common.DBWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SecurityDB {


    private static final Logger logger = LogManager.getLogger(SecurityDB.class);

    private static final String GET_USER_BY_USERNAME =
            "FROM User u " +
            "WHERE u.username = :username";


    private static final String GET_USERS =
            "FROM User";


    private static final String DELETE_USER_BY_USERNAME_QUERY =
            "DELETE FROM User " +
            "WHERE username = :username";


    @Autowired
    private DBWrapper dbWrapper;

    public User saveUser(User user) {

        ModelUtil.setFieldsForNewlyCreated(user);
        dbWrapper.save(user);
        return user;
    }

    public List<User> getAllUsers() {

        Session session = dbWrapper.startSession();
        Query query = session.createQuery(GET_USERS);
        List queryResult = query.list();
        dbWrapper.finishSession(session);
        return queryResult;
    }

    public User getUserByUsername(String username) {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(GET_USER_BY_USERNAME);
        query.setParameter("username", username);
        List queryResult = query.list();
        if(queryResult.isEmpty()) {
            return null;
        } else if(queryResult.size() > 1) {
            logger.error("There were more than 1 with the same id.");
        }
        dbWrapper.finishSession(session);
        return (User) queryResult.get(0);
    }

    public void deleteUserByUsername(String username) {
        Session session = dbWrapper.startSession();
        Query query = session.createQuery(DELETE_USER_BY_USERNAME_QUERY);
        query.setParameter("username", username);
        int i = query.executeUpdate();
        if(i > 1){
            logger.error("More than 1 item has been modified by delete. Items modified:" + i);
        } else if( i == 0 ){
            logger.error("Nothing has been deleted. This error checking should have been done at service level. " +
                    "Username:" + username);
        }
        dbWrapper.finishCommitSession(session);
    }


}
