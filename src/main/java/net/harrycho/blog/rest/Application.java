package net.harrycho.blog.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.Environment;

import java.io.PrintStream;

@EnableAutoConfiguration
@ComponentScan
@Configuration
//@AutoConfigureAfter
public class Application {

    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String [] args) {

        log.info("**************************************************************************************");
        log.info("************************* Web Application Started ************************************");

        SpringApplication app = new SpringApplication(Application.class);

        Banner banner = new Banner() {
            public void printBanner(Environment environment, Class<?> aClass, PrintStream printStream) {
                printStream.append("          _______  _______  _______             _______           _______  _  _______    ______   _        _______  _______ \n" +
                        "|\\     /|(  ___  )(  ____ )(  ____ )|\\     /|  (  ____ \\|\\     /|(  ___  )( )(  ____ \\  (  ___ \\ ( \\      (  ___  )(  ____ \\\n" +
                        "| )   ( || (   ) || (    )|| (    )|( \\   / )  | (    \\/| )   ( || (   ) ||/ | (    \\/  | (   ) )| (      | (   ) || (    \\/\n" +
                        "| (___) || (___) || (____)|| (____)| \\ (_) /   | |      | (___) || |   | |   | (_____   | (__/ / | |      | |   | || |      \n" +
                        "|  ___  ||  ___  ||     __)|     __)  \\   /    | |      |  ___  || |   | |   (_____  )  |  __ (  | |      | |   | || | ____ \n" +
                        "| (   ) || (   ) || (\\ (   | (\\ (      ) (     | |      | (   ) || |   | |         ) |  | (  \\ \\ | |      | |   | || | \\_  )\n" +
                        "| )   ( || )   ( || ) \\ \\__| ) \\ \\__   | |     | (____/\\| )   ( || (___) |   /\\____) |  | )___) )| (____/\\| (___) || (___) |\n" +
                        "|/     \\||/     \\||/   \\__/|/   \\__/   \\_/     (_______/|/     \\|(_______)   \\_______)  |/ \\___/ (_______/(_______)(_______)\n" +
                        "                                                                                                                            \n");
            }
        };

        app.setBanner(banner);

        app.run(args);
    }
}
